Rails.application.routes.draw do
  root 'home#index'
  
  resources :users do
      member do
          get 'Profile'
          get 'Matches'
      end
  end
  
end
